## Deep Speech 2: End-to-End Speech Recognition in English and Mandarin
Link: https://arxiv.org/pdf/1512.02595v1.pdf
Source code: https://github.com/tensorflow/models/tree/master/research/deep_speech

## THE PYTORCH-KALDI SPEECH RECOGNITION TOOLKIT
Link: https://arxiv.org/pdf/1811.07453v2.pdf
Source code: https://github.com/mravanelli/pytorch-kaldi

## Deep Speech: Scaling up end-to-end speech recognition
Link: https://arxiv.org/pdf/1412.5567.pdf
Source code: https://github.com/mozilla/DeepSpeech

## SpecAugment: A Simple Data Augmentation Method for Automatic Speech Recognition
Link: https://arxiv.org/pdf/1904.08779v2.pdf
Source code: https://github.com/DemisEom/SpecAugment

## wav2letter++: The Fastest Open-source Speech Recognition System
Link: https://arxiv.org/pdf/1812.07625v1.pdf
Source code: https://github.com/facebookresearch/wav2letter

## Jasper: An End-to-End Convolutional Neural Acoustic Model
Link: https://arxiv.org/pdf/1904.03288.pdf
Source code: https://github.com/NVIDIA/DeepLearningExamples/tree/master/PyTorch/SpeechRecognition/Jasper